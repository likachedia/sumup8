package com.example.sumup8.network


import com.example.sumup8.model.Courses
import retrofit2.Response
import retrofit2.http.GET

interface SimpleApi {

  @GET("v3/4167a598-b68c-420f-b6e1-fef68b89a10d")
    suspend fun getCustomPost(
    ): Response<Courses>


}