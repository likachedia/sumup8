package com.example.sumup8.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup8.databinding.ActivecourseModelFragmentBinding
import com.example.sumup8.extenstion.setImage
import com.example.sumup8.model.ActiveCourse
import okhttp3.internal.toHexString


class ActiveCourseAdapter(private val courses: List<ActiveCourse>): RecyclerView.Adapter<ActiveCourseAdapter.ActiveCourseViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActiveCourseAdapter.ActiveCourseViewHolder {
        return ActiveCourseViewHolder(ActivecourseModelFragmentBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ActiveCourseAdapter.ActiveCourseViewHolder, position: Int) {
        val model = courses[position]
        holder.onBind(model)
    }

    override fun getItemCount() = courses.size

    inner class ActiveCourseViewHolder(private val binding: ActivecourseModelFragmentBinding) : RecyclerView.ViewHolder(binding.root){

        fun onBind(model:ActiveCourse) {
            Log.i("size", courses.size.toString())
            val opPercent = setOpacity(model.backgroundColorPercent!!.toInt())
            val btnPercent = setOpacity(model.playButtonColorPercent!!.toInt())
            Log.i("color", opPercent)
            binding.imgIcon.setImage(model.image)
            binding.imgIcon.setBackgroundColor(Color.parseColor("#" + model.mainColor))
            binding.tvBooked.text = model.title.toString()
            binding.tvBooked.setTextColor(Color.parseColor("#" + model.mainColor.toString()))
            binding.tvBookedFaded.text = model.bookingTime.toString()
            binding.tvBookedFaded.setTextColor(Color.parseColor("#" + opPercent + model.mainColor))
            binding.constraint.setBackgroundColor(Color.parseColor("#" + opPercent + model.mainColor))

            binding.imgIcon.setBackgroundColor(Color.parseColor("#" + model.mainColor))
            binding.imgIcon.drawable.setTint(Color.parseColor("#" + model.mainColor))
            binding.imgPly.setBackgroundColor(Color.parseColor("#" + btnPercent + model.mainColor))
            binding.imgPly.drawable.setTint(Color.parseColor("#" + btnPercent + model.mainColor))
            binding.progressCircleDeterminate.progress = model.progress.toString().toInt()
            binding.progressCircleDeterminate.setIndicatorColor(Color.parseColor("#" + model.mainColor.toString()))
        }

        private fun setOpacity(percent: Int): String {
            val opacity = percent / 100.0
            val tr = opacity * 255
           return tr.toInt().toHexString()

        }
    }
}