package com.example.sumup8.resources

enum class IconType(val type: String) {
    SETTINGS("settings"),
    CARDS("card")
}