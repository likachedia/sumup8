package com.example.sumup8.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.sumup8.Repository
import com.example.sumup8.base.BaseViewModel
import com.example.sumup8.resources.Resource
import kotlinx.coroutines.launch

class CourseViewModel(private val repository: Repository): BaseViewModel(repository){


    private val _myResponse: MutableLiveData<UiState> = MutableLiveData()
    val myResponse: LiveData<UiState> get() = _myResponse

    init {
        fetchData()
    }


    private fun fetchData() {

        viewModelScope.launch {
            val data = repository.getPost()
            Log.i("data", data.data.toString());
            when(data) {
                is Resource.Success -> {
                    _myResponse.value = UiState(items = (data.data))
                    // Log.i("uistate-1", _uiState.toString())
                }
                is Resource.Error -> {
                    _myResponse.value = UiState(error = UiState.Error.ClientError)
                }
            }
        }
    }



    data class UiState(
        val isLoading: Boolean = false,
        val error:Error? = null,
        val items: Courses? = null
    ) {
        sealed class Error {
            object ClientError: Error()
        }
    }
}