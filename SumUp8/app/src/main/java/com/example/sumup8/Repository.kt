package com.example.sumup8

import android.util.Log
import com.example.sumup8.model.Courses
import com.example.sumup8.network.RetrofitInstance
import com.example.sumup8.resources.Resource
import okio.IOException
import retrofit2.HttpException
import retrofit2.Response

class Repository {

 /*   suspend fun pushPost(post:LoginRequest): Resource<LoginResponse> {
        return try {
            val response = RetrofitInstance.api.pushPost(post)
            Log.i("response: ", response.toString())
            val result = response.body()
            if(response.isSuccessful && result != null) {
                Log.i("response push: ", response.toString())
               Resource.Success(result)

            } else {
                Log.i("response: ", response.message().toString())
                Resource.Error("Something went wrong")
            }

        }catch (exception: IOException) {
            Resource.Error(exception.toString())
        } catch (exception: HttpException) {
            Resource.Error(exception.toString())
        }

    } */

    suspend fun getPost(): Resource<Courses> {
        return try {
            val response = RetrofitInstance.api.getCustomPost()
            Log.i("response: ", response.toString())
            val result = response.body()
            if(response.isSuccessful && result != null) {
                Log.i("response get: ", response.toString())
                Resource.Success(result)

            } else {
                Log.i("response get: ", response.message().toString())
                Resource.Error("Something went wrong")
            }

        }catch (exception: IOException) {
            Resource.Error(exception.toString())
        } catch (exception: HttpException) {
            Resource.Error(exception.toString())
        }
    }

}