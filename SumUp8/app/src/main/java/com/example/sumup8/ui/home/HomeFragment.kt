package com.example.sumup8.ui.home

import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup8.NewCourseAdapter
import com.example.sumup8.R
import com.example.sumup8.adapter.ActiveCourseAdapter
import com.example.sumup8.base.BaseFragment
import com.example.sumup8.databinding.FragmentHomeBinding
import com.example.sumup8.model.ActiveCourse
import com.example.sumup8.model.CourseViewModel
import com.example.sumup8.model.NewCourse
import com.example.sumup8.resources.Resource
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : BaseFragment<FragmentHomeBinding, CourseViewModel>(FragmentHomeBinding::inflate) {
    private lateinit var cadapter: NewCourseAdapter
    private lateinit var aadapter: ActiveCourseAdapter
    private lateinit var recycler: RecyclerView
    override fun getViewModelClass() = CourseViewModel::class.java

    override fun start() {
        observer()
        listeners()
    }
    private fun listeners() {
        binding.root.setOnClickListener {
            val nav = activity?.findViewById<BottomNavigationView>(R.id.nav_view)
            Log.i("visibility", "check")
            if(nav!!.isVisible) {
                Log.i("visibility", "gone")
                nav.visibility = View.GONE

            } else {
                Log.i("visibility", "visible")
                nav.visibility = View.VISIBLE

            }

        }
    }
    private fun initRec(course: List<NewCourse>) {
        cadapter = NewCourseAdapter(course)
        binding.recyclerNew.apply {
            adapter = cadapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }
    }

    private fun initActiveRec(course: List<ActiveCourse>) {
        aadapter = ActiveCourseAdapter(course)
        binding.recyclerActive.apply {
            adapter = aadapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }
    private fun observer() {
        viewModel.myResponse.observe(viewLifecycleOwner, {
            if(it.items != null) {
                initRec(it.items.newCourses!!)
                initActiveRec(it.items.activeCourses!!)
            }
            if(it.error != null) {
               CourseViewModel.UiState.Error.ClientError
            }

        })
    }
}