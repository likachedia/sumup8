package com.example.sumup8

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup8.databinding.NewcourseModelFragmentBinding
import com.example.sumup8.model.NewCourse
import com.example.sumup8.resources.IconType
import java.util.concurrent.TimeUnit

class NewCourseAdapter(private val courses: List<NewCourse>): RecyclerView.Adapter<NewCourseAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewCourseAdapter.ViewHolder {
        return ViewHolder(NewcourseModelFragmentBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: NewCourseAdapter.ViewHolder, position: Int) {
        val model = courses[position]
        holder.onBind(model)
    }

    override fun getItemCount() = courses.size

    inner class ViewHolder(private val binding:NewcourseModelFragmentBinding): RecyclerView.ViewHolder(binding.root) {

        fun onBind(model: NewCourse) {
            if(model.iconType == IconType.SETTINGS.type) {
                    binding.imIcon.setImageResource(R.drawable.ic_settings)
                } else if(model.iconType == IconType.CARDS.type){
                binding.imIcon.setImageResource(R.drawable.ic_shape)
            }

            if(model.mainColor == "3DD598") {
                binding.imStart.setImageResource(R.drawable.ic_triangle_green)
            }
            binding.tvIntroduce.text = model.title.toString()
            binding.tvQuestion.text = model.question.toString()
            binding.tvDate.text = TimeUnit.MILLISECONDS.toSeconds(model.duration?.toLong()!!).toString().plus("  min")
            binding.constraintnt.setBackgroundColor(Color.parseColor("#" + model.mainColor.toString()))
        }
    }
}