package com.example.sumup8

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sumup8.model.CourseViewModel

class CourseViewModelFactory(private val repository: Repository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return  CourseViewModel(repository) as T
    }
}