package com.example.sumup8.resources

import android.app.Application
import android.content.Context

class App :Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext

    }
    companion object {
        var appContext: Context? = null
    }
}