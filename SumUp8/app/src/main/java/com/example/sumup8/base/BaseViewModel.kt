package com.example.sumup8.base


import androidx.lifecycle.ViewModel
import com.example.sumup8.Repository


abstract class BaseViewModel(private val repository: Repository): ViewModel()